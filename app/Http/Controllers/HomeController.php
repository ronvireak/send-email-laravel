<?php

namespace App\Http\Controllers;

use App\Mail\SendMailable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function mail(){
        $name = 'Ron Vireak';
        Mail::to('ronvireak96@gmail.com')->send(new SendMailable($name));

        return 'Email was sent successfully';
    }
}
